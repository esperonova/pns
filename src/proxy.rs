use crate::protocol::*;
use md5::Digest;
use std::borrow::Cow;
use std::ops::Deref;
use tokio::io::AsyncWriteExt;

async fn establish_upstream_connection() -> Result<tokio::net::TcpStream, anyhow::Error> {
    Ok(tokio::net::TcpStream::connect(
        match std::env::var("PNS_UPSTREAM_HOST") {
            Ok(value) => Cow::Owned(value),
            Err(std::env::VarError::NotPresent) => Cow::Borrowed("127.0.0.1:25565"),
            Err(err) => return Err(err.into()),
        }
        .deref(),
    )
    .await?)
}

async fn handle_connection(
    mut socket: tokio::net::TcpStream,
    socket_addr: std::net::SocketAddr,
    db: sled::Db,
) -> Result<(), anyhow::Error> {
    let mut buf = bytes::BytesMut::new();
    let first_packet = read_packet(&mut socket, &mut buf, false).await?;

    println!("packet = {:?}", first_packet);

    if first_packet.packet_id != 0 {
        anyhow::bail!("unexpected packet during handshake");
    }

    let mut pbuf = first_packet.data;

    let version_code = read_varint(None::<&mut tokio::net::TcpStream>, &mut pbuf).await?;
    let version = match ProtocolVersion::try_parse(version_code) {
        Some(version) => version,
        None => {
            let mut kick_packet_buf = bytes::BytesMut::new();
            write_string(&mut kick_packet_buf, r#"{"text": "Malĝusta versio."}"#);
            send_packet(&mut socket, 0x00, &kick_packet_buf, false).await?;
            return Ok(());
        }
    };

    let host = read_string(&mut pbuf, 255).await?;

    let port = read_ushort(&mut pbuf)?;

    if pbuf.len() != 1 {
        anyhow::bail!("failed to read packet");
    }

    match pbuf[0] {
        1 => {
            let mut out_handshake_packet_buf = bytes::BytesMut::new();
            write_varint(&mut out_handshake_packet_buf, version_code);
            write_string(&mut out_handshake_packet_buf, &host);
            write_ushort(&mut out_handshake_packet_buf, port);
            write_varint(&mut out_handshake_packet_buf, 1);

            let mut out_conn = establish_upstream_connection().await?;
            send_packet(&mut out_conn, 0, &out_handshake_packet_buf, false).await?;

            println!("forwarding");

            // start forwarding
            out_conn.write_all(&buf).await?;

            let (mut socket_reader, mut socket_writer) = tokio::io::split(socket);
            let (mut out_conn_reader, mut out_conn_writer) = tokio::io::split(out_conn);

            futures_util::try_join!(
                tokio::io::copy(&mut socket_reader, &mut out_conn_writer),
                tokio::io::copy(&mut out_conn_reader, &mut socket_writer),
            )?;
        }
        2 => {
            let login_start_packet = read_packet(&mut socket, &mut buf, false).await?;

            if login_start_packet.packet_id != 0 {
                anyhow::bail!("unexpected packet at login start");
            }

            let mut pbuf = login_start_packet.data;

            let req_username = read_string(&mut pbuf, 16).await?;
            if req_username.len() > 15 {
                anyhow::bail!("la uzantnomo estas tro longa");
            }
            for ch in req_username.chars() {
                if !(ch.is_alphanumeric() || ch == '_') {
                    anyhow::bail!("la uzantnomo enhavas malpermesitajn simbolojn");
                }
            }

            let username = format!("-{}", req_username);
            let lookup_username = req_username.to_ascii_lowercase();

            let mut hasher = md5::Md5::new();

            hasher.update("OfflinePlayer:".as_bytes());
            hasher.update(&username);

            let mut uuid_bytes = [0; 16];
            uuid_bytes.copy_from_slice(&hasher.finalize()[..16]);

            let uuid = uuid::Builder::from_md5_bytes(uuid_bytes).into_uuid();

            let mut set_compression_packet_buf = bytes::BytesMut::new();
            write_varint(&mut set_compression_packet_buf, 512);

            send_packet(&mut socket, 0x03, &set_compression_packet_buf, false).await?;

            let mut login_success_packet_buf = bytes::BytesMut::new();
            write_uuid(&mut login_success_packet_buf, uuid);
            write_string(&mut login_success_packet_buf, &username);
            write_varint(&mut login_success_packet_buf, 0);

            match version {
                ProtocolVersion::V1_19_1
                | ProtocolVersion::V1_19_3
                | ProtocolVersion::V1_19_4
                | ProtocolVersion::V1_20
                | ProtocolVersion::V1_20_4 => {}
                ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {
                    write_bool(&mut login_success_packet_buf, true);
                }
            }

            send_packet(&mut socket, 0x02, &login_success_packet_buf, true).await?;

            match version {
                ProtocolVersion::V1_19_1
                | ProtocolVersion::V1_19_3
                | ProtocolVersion::V1_19_4
                | ProtocolVersion::V1_20 => {}
                ProtocolVersion::V1_20_4 => {
                    // in Configuration state now

                    send_packet(
                        &mut socket,
                        0x05,
                        &include_bytes!("./registry_codec_1_20_4.nbt")[..],
                        true,
                    )
                    .await?;

                    send_packet(&mut socket, 0x02, &[], true).await?;
                }
                ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {
                    let mut known_packs_packet_buf = bytes::BytesMut::new();
                    write_varint(&mut known_packs_packet_buf, 1);
                    write_string(&mut known_packs_packet_buf, "minecraft");
                    write_string(&mut known_packs_packet_buf, "core");
                    write_string(
                        &mut known_packs_packet_buf,
                        match version {
                            ProtocolVersion::V1_19_1
                            | ProtocolVersion::V1_19_3
                            | ProtocolVersion::V1_19_4
                            | ProtocolVersion::V1_20
                            | ProtocolVersion::V1_20_4 => unreachable!(),
                            ProtocolVersion::V1_20_6 => "1.20.6",
                            ProtocolVersion::V1_21 => "1.21.1",
                        },
                    );

                    send_packet(&mut socket, 0x0e, &known_packs_packet_buf, true).await?;

                    let mut src = bytes::BytesMut::from(match version {
                        ProtocolVersion::V1_19_1
                        | ProtocolVersion::V1_19_3
                        | ProtocolVersion::V1_19_4
                        | ProtocolVersion::V1_20
                        | ProtocolVersion::V1_20_4 => unreachable!(),
                        ProtocolVersion::V1_20_6 => {
                            &include_bytes!("./registry_codec_1_20_6.dat")[..]
                        }
                        ProtocolVersion::V1_21 => {
                            &include_bytes!("./registry_codec_1_21_1.dat")[..]
                        }
                    });

                    // codec file was retrieved from a server without compression enabled, so need
                    // to adjust it a bit
                    while !src.is_empty() {
                        let mut content =
                            read_packet_bytes(None::<&mut tokio::net::TcpStream>, &mut src).await?;
                        let packet_id =
                            read_varint(None::<&mut tokio::net::TcpStream>, &mut content).await?
                                as i8;

                        send_packet(&mut socket, packet_id, &content, true).await?;
                    }

                    send_packet(&mut socket, 0x03, &[], true).await?;
                }
            }

            let mut play_login_packet_buf = bytes::BytesMut::new();
            write_int(&mut play_login_packet_buf, 0); // Entity ID
            write_bool(&mut play_login_packet_buf, false); // Is hardcore

            match version {
                ProtocolVersion::V1_19_1
                | ProtocolVersion::V1_19_3
                | ProtocolVersion::V1_19_4
                | ProtocolVersion::V1_20 => {
                    write_ubyte(&mut play_login_packet_buf, 3); // Gamemode
                    write_byte(&mut play_login_packet_buf, -1); // Previous Gamemode
                }
                ProtocolVersion::V1_20_4 | ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {}
            }

            write_varint(&mut play_login_packet_buf, 0); // Dimension Count
            match version {
                ProtocolVersion::V1_19_1 | ProtocolVersion::V1_19_3 => {
                    play_login_packet_buf
                        .extend_from_slice(&include_bytes!("./registry_codec_1_19_1.nbt")[..]);
                }
                ProtocolVersion::V1_19_4 => play_login_packet_buf
                    .extend_from_slice(&include_bytes!("./registry_codec_1_19_4.nbt")[..]),
                ProtocolVersion::V1_20 => play_login_packet_buf
                    .extend_from_slice(&include_bytes!("./registry_codec_1_20_1.nbt")[..]),
                ProtocolVersion::V1_20_4 | ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {}
            }

            match version {
                ProtocolVersion::V1_19_1
                | ProtocolVersion::V1_19_3
                | ProtocolVersion::V1_19_4
                | ProtocolVersion::V1_20 => {
                    write_string(&mut play_login_packet_buf, "minecraft:overworld"); // Dimension Type
                    write_string(&mut play_login_packet_buf, "minecraft:overworld"); // Dimension Name
                    write_long(&mut play_login_packet_buf, 0); // Hashed seed
                    write_varint(&mut play_login_packet_buf, 0); // Max Players
                    write_varint(&mut play_login_packet_buf, 2); // View Distance
                    write_varint(&mut play_login_packet_buf, 0); // Simulation Distance
                    write_bool(&mut play_login_packet_buf, false); // Reduced Debug Info
                    write_bool(&mut play_login_packet_buf, false); // Enable respawn screen
                }
                ProtocolVersion::V1_20_4 | ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {
                    write_varint(&mut play_login_packet_buf, 0); // Max Players
                    write_varint(&mut play_login_packet_buf, 2); // View Distance
                    write_varint(&mut play_login_packet_buf, 0); // Simulation Distance
                    write_bool(&mut play_login_packet_buf, false); // Reduced Debug Info
                    write_bool(&mut play_login_packet_buf, false); // Enable respawn screen
                    write_bool(&mut play_login_packet_buf, false); // Do limited crafting

                    match version {
                        ProtocolVersion::V1_19_1
                        | ProtocolVersion::V1_19_3
                        | ProtocolVersion::V1_19_4
                        | ProtocolVersion::V1_20 => unimplemented!(),
                        ProtocolVersion::V1_20_4 => {
                            write_string(&mut play_login_packet_buf, "minecraft:overworld");
                            // Dimension Type
                        }
                        ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {
                            write_varint(&mut play_login_packet_buf, 0); // Dimension Type
                        }
                    }

                    write_string(&mut play_login_packet_buf, "minecraft:overworld"); // Dimension Name
                    write_long(&mut play_login_packet_buf, 0); // Hashed seed
                    write_ubyte(&mut play_login_packet_buf, 3); // Gamemode
                    write_byte(&mut play_login_packet_buf, -1); // Previous Gamemode
                }
            }

            write_bool(&mut play_login_packet_buf, false); // Is Debug
            write_bool(&mut play_login_packet_buf, false); // Is Flat
            write_bool(&mut play_login_packet_buf, false); // Has death location
            match version {
                ProtocolVersion::V1_19_1 | ProtocolVersion::V1_19_3 | ProtocolVersion::V1_19_4 => {}
                ProtocolVersion::V1_20
                | ProtocolVersion::V1_20_4
                | ProtocolVersion::V1_20_6
                | ProtocolVersion::V1_21 => {
                    write_varint(&mut play_login_packet_buf, 0); // Portal cooldown
                }
            }
            match version {
                ProtocolVersion::V1_19_1
                | ProtocolVersion::V1_19_3
                | ProtocolVersion::V1_19_4
                | ProtocolVersion::V1_20
                | ProtocolVersion::V1_20_4 => {}
                ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {
                    write_bool(&mut play_login_packet_buf, false); // Enforces Secure Chat
                }
            }

            send_packet(
                &mut socket,
                match version {
                    ProtocolVersion::V1_19_1 => 0x25,
                    ProtocolVersion::V1_19_3 => 0x24,
                    ProtocolVersion::V1_19_4 | ProtocolVersion::V1_20 => 0x28,
                    ProtocolVersion::V1_20_4 => 0x29,
                    ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x2b,
                },
                &play_login_packet_buf,
                true,
            )
            .await?;

            match version {
                ProtocolVersion::V1_19_1
                | ProtocolVersion::V1_19_3
                | ProtocolVersion::V1_19_4
                | ProtocolVersion::V1_20 => {}
                ProtocolVersion::V1_20_4 | ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {
                    let mut game_event_packet_buf = bytes::BytesMut::new();
                    write_ubyte(&mut game_event_packet_buf, 13); // "Start waiting for level
                                                                 // chunks"
                    write_float(&mut game_event_packet_buf, 0.0);

                    send_packet(
                        &mut socket,
                        match version {
                            ProtocolVersion::V1_19_1
                            | ProtocolVersion::V1_19_3
                            | ProtocolVersion::V1_19_4
                            | ProtocolVersion::V1_20 => unreachable!(),
                            ProtocolVersion::V1_20_4 => 0x20,
                            ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x22,
                        },
                        &game_event_packet_buf,
                        true,
                    )
                    .await?;
                }
            }

            let mut spawn_position_packet_buf = bytes::BytesMut::new();
            write_position(&mut spawn_position_packet_buf, 0, 0, 0);
            write_float(&mut spawn_position_packet_buf, 0.0);

            send_packet(
                &mut socket,
                match version {
                    ProtocolVersion::V1_19_1 => 0x4d,
                    ProtocolVersion::V1_19_3 => 0x4c,
                    ProtocolVersion::V1_19_4 | ProtocolVersion::V1_20 => 0x50,
                    ProtocolVersion::V1_20_4 => 0x54,
                    ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x56,
                },
                &spawn_position_packet_buf,
                true,
            )
            .await?;

            let mut sync_position_packet_buf = bytes::BytesMut::new();
            write_double(&mut sync_position_packet_buf, 0.0);
            write_double(&mut sync_position_packet_buf, -128.0);
            write_double(&mut sync_position_packet_buf, 0.0);
            write_float(&mut sync_position_packet_buf, 0.0);
            write_float(&mut sync_position_packet_buf, 0.0);
            write_byte(&mut sync_position_packet_buf, 0);
            write_varint(&mut sync_position_packet_buf, 0);
            match version {
                ProtocolVersion::V1_19_1 | ProtocolVersion::V1_19_3 => {
                    write_bool(&mut sync_position_packet_buf, false);
                }
                ProtocolVersion::V1_19_4
                | ProtocolVersion::V1_20
                | ProtocolVersion::V1_20_4
                | ProtocolVersion::V1_20_6
                | ProtocolVersion::V1_21 => {}
            }

            send_packet(
                &mut socket,
                match version {
                    ProtocolVersion::V1_19_1 => 0x39,
                    ProtocolVersion::V1_19_3 => 0x38,
                    ProtocolVersion::V1_19_4 | ProtocolVersion::V1_20 => 0x3c,
                    ProtocolVersion::V1_20_4 => 0x3e,
                    ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x40,
                },
                &sync_position_packet_buf,
                true,
            )
            .await?;

            let passhash = db.get(&lookup_username)?;
            let mut passhash = passhash
                .as_deref()
                .map(|src| String::from_utf8(src.to_vec()))
                .transpose()?;

            send_chat_packet(&mut socket, true, version, "Entajpu pasvorton por daŭrigi.").await?;

            loop {
                let packet = read_packet(&mut socket, &mut buf, true).await?;

                if packet.packet_id
                    == match version {
                        ProtocolVersion::V1_19_1
                        | ProtocolVersion::V1_19_3
                        | ProtocolVersion::V1_19_4
                        | ProtocolVersion::V1_20
                        | ProtocolVersion::V1_20_4 => 0x05,
                        ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x06,
                    }
                {
                    let mut content = packet.data;
                    let message = read_string(&mut content, 256).await?;

                    let ok = if let Some(hash) = passhash {
                        let (ok, passhash_) = tokio::task::spawn_blocking(|| {
                            let ok = bcrypt::verify(message, &hash);
                            (ok, hash)
                        })
                        .await?;

                        passhash = Some(passhash_);

                        ok?
                    } else {
                        let passhash: String = tokio::task::spawn_blocking(|| {
                            bcrypt::hash(message, bcrypt::DEFAULT_COST)
                        })
                        .await??;

                        db.insert(&lookup_username, passhash.as_bytes())?;

                        true
                    };

                    if ok {
                        let mut out_conn = establish_upstream_connection().await?;

                        let host_for_upstream = format!("{}\0{}\0{}", host, socket_addr.ip(), uuid);

                        let mut out_handshake_packet_buf = bytes::BytesMut::new();
                        write_varint(&mut out_handshake_packet_buf, version_code);
                        write_string(&mut out_handshake_packet_buf, &host_for_upstream);
                        write_ushort(&mut out_handshake_packet_buf, port);
                        write_varint(&mut out_handshake_packet_buf, 2);

                        send_packet(&mut out_conn, 0, &out_handshake_packet_buf, false).await?;

                        let mut out_login_start_packet_buf = bytes::BytesMut::new();
                        write_string(&mut out_login_start_packet_buf, &username);
                        match version {
                            ProtocolVersion::V1_19_1 => {
                                write_bool(&mut out_login_start_packet_buf, false);
                            }
                            ProtocolVersion::V1_19_3
                            | ProtocolVersion::V1_19_4
                            | ProtocolVersion::V1_20
                            | ProtocolVersion::V1_20_4
                            | ProtocolVersion::V1_20_6
                            | ProtocolVersion::V1_21 => {}
                        }
                        match version {
                            ProtocolVersion::V1_19_1
                            | ProtocolVersion::V1_19_3
                            | ProtocolVersion::V1_19_4
                            | ProtocolVersion::V1_20 => {
                                write_bool(&mut out_login_start_packet_buf, true);
                            }
                            ProtocolVersion::V1_20_4
                            | ProtocolVersion::V1_20_6
                            | ProtocolVersion::V1_21 => {}
                        }
                        write_uuid(&mut out_login_start_packet_buf, uuid);

                        send_packet(&mut out_conn, 0, &out_login_start_packet_buf, false).await?;

                        let mut upstream_buf = bytes::BytesMut::new();

                        let mut got_compression_packet = false;

                        loop {
                            let upstream_packet = read_packet(
                                &mut out_conn,
                                &mut upstream_buf,
                                got_compression_packet,
                            )
                            .await?;
                            println!("got upstream packet {}", upstream_packet.packet_id);
                            if upstream_packet.packet_id == 0x03 {
                                got_compression_packet = true;
                            } else if upstream_packet.packet_id == 0x02 {
                                break;
                            } else if upstream_packet.packet_id == 0x00 {
                                // disconnected, forward message to client

                                send_packet(
                                    &mut socket,
                                    match version {
                                        ProtocolVersion::V1_19_1 => 0x19,
                                        ProtocolVersion::V1_19_3 => 0x17,
                                        ProtocolVersion::V1_19_4 | ProtocolVersion::V1_20 => 0x1a,
                                        ProtocolVersion::V1_20_4 => 0x1b,
                                        ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x1d,
                                    },
                                    &upstream_packet.data,
                                    true,
                                )
                                .await?;

                                return Ok(());
                            } else {
                                anyhow::bail!(
                                    "unexpected packet from upstream: {} {:?}",
                                    upstream_packet.packet_id,
                                    upstream_packet.data
                                );
                            }
                        }

                        if !got_compression_packet {
                            anyhow::bail!("missing compression upstream");
                        }

                        match version {
                            ProtocolVersion::V1_19_1
                            | ProtocolVersion::V1_19_3
                            | ProtocolVersion::V1_19_4
                            | ProtocolVersion::V1_20 => {
                                // wait for next packet (should be 0x25) before forwarding
                                let next_packet_bytes =
                                    read_packet_bytes(Some(&mut out_conn), &mut upstream_buf)
                                        .await?;

                                let mut len_buf = bytes::BytesMut::new();
                                write_varint(&mut len_buf, next_packet_bytes.len() as i32);
                                socket.write_all(&len_buf).await?;
                                socket.write_all(&next_packet_bytes).await?;

                                let mut respawn_packet_buf = bytes::BytesMut::new();
                                write_string(&mut respawn_packet_buf, "minecraft:overworld"); // Dimension Type
                                write_string(&mut respawn_packet_buf, "minecraft:overworld"); // dimension
                                                                                              // name
                                write_long(&mut respawn_packet_buf, 0); // hashed seed
                                write_ubyte(&mut respawn_packet_buf, 0); // game mode
                                write_byte(&mut respawn_packet_buf, -1); // previous game mode
                                write_bool(&mut respawn_packet_buf, false); // is debug
                                write_bool(&mut respawn_packet_buf, false); // is flat
                                match version {
                                    ProtocolVersion::V1_19_1 | ProtocolVersion::V1_19_3 => {
                                        write_bool(&mut respawn_packet_buf, false);
                                    }
                                    ProtocolVersion::V1_19_4 | ProtocolVersion::V1_20 => {
                                        write_byte(&mut respawn_packet_buf, 0); // data kept
                                    }
                                    ProtocolVersion::V1_20_4
                                    | ProtocolVersion::V1_20_6
                                    | ProtocolVersion::V1_21 => {
                                        unreachable!()
                                    }
                                }
                                write_bool(&mut respawn_packet_buf, false); // has death location
                                match version {
                                    ProtocolVersion::V1_19_1
                                    | ProtocolVersion::V1_19_3
                                    | ProtocolVersion::V1_19_4 => {}
                                    ProtocolVersion::V1_20 => {
                                        write_varint(&mut respawn_packet_buf, 0);
                                        // portal cooldown
                                    }
                                    ProtocolVersion::V1_20_4
                                    | ProtocolVersion::V1_20_6
                                    | ProtocolVersion::V1_21 => {
                                        unreachable!()
                                    }
                                }

                                send_packet(
                                    &mut socket,
                                    match version {
                                        ProtocolVersion::V1_19_1 => 0x3e,
                                        ProtocolVersion::V1_19_3 => 0x3d,
                                        ProtocolVersion::V1_19_4 | ProtocolVersion::V1_20 => 0x41,
                                        ProtocolVersion::V1_20_4
                                        | ProtocolVersion::V1_20_6
                                        | ProtocolVersion::V1_21 => {
                                            unreachable!()
                                        }
                                    },
                                    &respawn_packet_buf,
                                    true,
                                )
                                .await?;
                            }
                            ProtocolVersion::V1_20_4
                            | ProtocolVersion::V1_20_6
                            | ProtocolVersion::V1_21 => {
                                // send Login Acknowledged
                                send_packet(&mut out_conn, 0x03, &[], true).await?;

                                // put client back into Configuration state
                                send_packet(
                                    &mut socket,
                                    match version {
                                        ProtocolVersion::V1_19_1
                                        | ProtocolVersion::V1_19_3
                                        | ProtocolVersion::V1_19_4
                                        | ProtocolVersion::V1_20 => unreachable!(),
                                        ProtocolVersion::V1_20_4 => 0x67,
                                        ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x69,
                                    },
                                    &[],
                                    true,
                                )
                                .await?;

                                // wait for Acknowledge Configuration
                                loop {
                                    let mut content =
                                        read_packet_bytes(Some(&mut socket), &mut buf).await?;

                                    let data_len = read_varint(
                                        None::<&mut tokio::net::TcpStream>,
                                        &mut content,
                                    )
                                    .await?;

                                    if data_len == 0 {
                                        let packet_id = read_varint(
                                            None::<&mut tokio::net::TcpStream>,
                                            &mut content,
                                        )
                                        .await?;
                                        if packet_id
                                            == match version {
                                                ProtocolVersion::V1_19_1
                                                | ProtocolVersion::V1_19_3
                                                | ProtocolVersion::V1_19_4
                                                | ProtocolVersion::V1_20 => unreachable!(),
                                                ProtocolVersion::V1_20_4 => 0x0B,
                                                ProtocolVersion::V1_20_6
                                                | ProtocolVersion::V1_21 => 0x0C,
                                            }
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        // start forwarding
                        out_conn.write_all(&buf).await?;

                        println!("forwarding");
                        socket.write_all(&upstream_buf).await?;

                        let (mut socket_reader, mut socket_writer) = tokio::io::split(socket);
                        let (mut out_conn_reader, mut out_conn_writer) = tokio::io::split(out_conn);

                        futures_util::try_join!(
                            tokio::io::copy(&mut socket_reader, &mut out_conn_writer),
                            tokio::io::copy(&mut out_conn_reader, &mut socket_writer),
                        )?;

                        break;
                    } else {
                        send_chat_packet(&mut socket, true, version, "Malĝusta pasvorto.").await?;
                    }
                }
            }
        }
        _ => anyhow::bail!("unknown next state"),
    }

    Ok(())
}

pub async fn run(db: &sled::Db) -> Result<(), anyhow::Error> {
    let listener = tokio::net::TcpListener::bind("0.0.0.0:25566").await?;

    loop {
        match listener.accept().await {
            Ok((socket, socket_addr)) => {
                let db = db.clone();
                tokio::spawn(async move {
                    println!("handling connection");
                    if let Err(err) = handle_connection(socket, socket_addr, db).await {
                        eprintln!("Failed while handling connection: {:?}", err);
                    }
                });
            }
            Err(err) => eprintln!("Failed to accept client: {:?}", err),
        }
    }
}
