use bytes::BufMut;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

const SEGMENT_BITS: u8 = 0x7F;
const CONTINUE_BIT: u8 = 0x80;

pub async fn read_varint(
    mut socket: Option<&mut (impl tokio::io::AsyncRead + std::marker::Unpin)>,
    buf: &mut bytes::BytesMut,
) -> Result<i32, anyhow::Error> {
    let mut value: i32 = 0;
    let mut position = 0;

    let mut bytes_read = 0;

    loop {
        let byte = if bytes_read < buf.len() {
            let byte = buf[bytes_read];
            bytes_read += 1;
            byte
        } else {
            buf.clear();
            socket
                .as_mut()
                .ok_or(anyhow::anyhow!("eof"))?
                .read_buf(buf)
                .await?;

            if buf.is_empty() {
                anyhow::bail!("eof");
            }

            bytes_read = 1;

            buf[0]
        };

        value |= i32::from(byte & SEGMENT_BITS) << position;

        if byte & CONTINUE_BIT == 0 {
            break;
        }

        position += 7;
        if position >= 32 {
            anyhow::bail!("varint is too long");
        }
    }

    let _ = buf.split_to(bytes_read);

    Ok(value)
}

pub async fn read_packet_bytes(
    mut socket: Option<&mut (impl tokio::io::AsyncRead + std::marker::Unpin)>,
    buf: &mut bytes::BytesMut,
) -> Result<bytes::BytesMut, anyhow::Error> {
    let len = read_varint(socket.as_mut(), buf).await?;

    if len < 1 {
        anyhow::bail!("invalid packet length");
    }

    let len = len as usize;

    while buf.len() < len {
        if let Some(socket) = &mut socket {
            socket.read_buf(buf).await?;
        } else {
            anyhow::bail!("eof");
        }
    }

    Ok(buf.split_to(len))
}

#[derive(Debug)]
pub struct RawPacket {
    pub packet_id: i32,
    pub data: bytes::BytesMut,
}

pub async fn read_packet(
    socket: &mut tokio::net::TcpStream,
    buf: &mut bytes::BytesMut,
    compression_enabled: bool,
) -> Result<RawPacket, anyhow::Error> {
    let mut content = read_packet_bytes(Some(socket), buf).await?;
    if compression_enabled {
        let data_len = read_varint(None::<&mut tokio::net::TcpStream>, &mut content).await?;
        println!("data_len={}", data_len);
        if data_len != 0 {
            anyhow::bail!("compression is not implemented");
        }
    }

    let packet_id = read_varint(None::<&mut tokio::net::TcpStream>, &mut content).await?;

    Ok(RawPacket {
        packet_id,
        data: content,
    })
}

pub async fn read_string(
    buf: &mut bytes::BytesMut,
    max_len: usize,
) -> Result<String, anyhow::Error> {
    let len = read_varint(None::<&mut tokio::net::TcpStream>, buf).await?;
    if len < 0 {
        anyhow::bail!("invalid length");
    }

    let len = len as usize;

    if len > max_len * 4 {
        // max size of each character is 4 bytes, so in this case it's definitely too long
        anyhow::bail!("string is longer than max_len");
    }
    if len > buf.len() {
        anyhow::bail!("eof");
    }
    let src = buf.split_to(len);

    let result = String::from_utf8(src.to_vec())?;
    if result.len() > max_len {
        anyhow::bail!("string is longer than max_len");
    }

    Ok(result)
}

pub fn read_ushort(buf: &mut bytes::BytesMut) -> Result<u16, anyhow::Error> {
    if buf.len() < 2 {
        anyhow::bail!("eof");
    }
    let src = buf.split_to(2);
    Ok(u16::from_be_bytes([src[0], src[1]]))
}

pub fn write_varint(buf: &mut bytes::BytesMut, value: i32) {
    let mut value = u32::from_be_bytes(value.to_be_bytes());
    loop {
        if value & !(SEGMENT_BITS as u32) == 0 {
            buf.put_u8(value as u8);
            return;
        }

        buf.put_u8(((value as u8) & SEGMENT_BITS) | CONTINUE_BIT);

        value >>= 7;
    }
}

pub fn write_string(buf: &mut bytes::BytesMut, value: &str) {
    let len = value.len();
    write_varint(buf, len as i32);
    buf.extend_from_slice(value.as_bytes());
}

pub fn write_ushort(buf: &mut bytes::BytesMut, value: u16) {
    buf.put_u16(value);
}

pub fn write_uuid(buf: &mut bytes::BytesMut, value: uuid::Uuid) {
    buf.extend_from_slice(&value.as_bytes()[..]);
}

pub fn write_int(buf: &mut bytes::BytesMut, value: i32) {
    buf.put_i32(value);
}

pub fn write_bool(buf: &mut bytes::BytesMut, value: bool) {
    buf.put_u8(if value { 1 } else { 0 });
}

pub fn write_ubyte(buf: &mut bytes::BytesMut, value: u8) {
    buf.put_u8(value);
}

pub fn write_byte(buf: &mut bytes::BytesMut, value: i8) {
    buf.put_i8(value);
}

pub fn write_long(buf: &mut bytes::BytesMut, value: i64) {
    buf.put_i64(value);
}

pub fn write_double(buf: &mut bytes::BytesMut, value: f64) {
    buf.put_f64(value);
}

pub fn write_float(buf: &mut bytes::BytesMut, value: f32) {
    buf.put_f32(value);
}

pub fn write_position(buf: &mut bytes::BytesMut, x: i64, y: i64, z: i64) {
    let value = ((x & 0x3FFFFFF) << 38) | ((z & 0x3FFFFFF) << 12) | (y & 0xFFF);
    buf.put_i64(value);
}

pub fn write_nbt_string(buf: &mut bytes::BytesMut, text: &str) {
    buf.put_u8(8);
    write_ushort(buf, text.len() as u16);
    buf.extend_from_slice(text.as_bytes());
}

pub async fn send_packet(
    socket: &mut tokio::net::TcpStream,
    packet_id: i8,
    content: &[u8],
    compression_enabled: bool,
) -> Result<(), anyhow::Error> {
    assert!(packet_id >= 0);

    println!("sending packet {} {:?}", packet_id, content);

    let mut len_buf = bytes::BytesMut::new();
    write_varint(
        &mut len_buf,
        (content.len() + if compression_enabled { 2 } else { 1 }) as i32,
    );
    if compression_enabled {
        len_buf.put_u8(0);
    }
    len_buf.put_u8(packet_id as u8);

    socket.write_all(&len_buf).await?;
    socket.write_all(&content).await?;

    Ok(())
}

#[derive(Clone, Copy)]
pub enum ProtocolVersion {
    V1_19_1,
    V1_19_3,
    V1_19_4,
    V1_20,
    V1_20_4,
    V1_20_6,
    V1_21,
}

impl ProtocolVersion {
    pub fn try_parse(version_code: i32) -> Option<Self> {
        match version_code {
            760 => Some(ProtocolVersion::V1_19_1),
            761 => Some(ProtocolVersion::V1_19_3),
            762 => Some(ProtocolVersion::V1_19_4),
            763 => Some(ProtocolVersion::V1_20),
            765 => Some(ProtocolVersion::V1_20_4),
            766 => Some(ProtocolVersion::V1_20_6),
            767 => Some(ProtocolVersion::V1_21),
            _ => None,
        }
    }
}

pub async fn send_chat_packet(
    socket: &mut tokio::net::TcpStream,
    compression_enabled: bool,
    version: ProtocolVersion,
    text: &str,
) -> Result<(), anyhow::Error> {
    let mut chat_packet_buf = bytes::BytesMut::new();

    match version {
        ProtocolVersion::V1_19_1
        | ProtocolVersion::V1_19_3
        | ProtocolVersion::V1_19_4
        | ProtocolVersion::V1_20 => {
            let content = format!(r#"{{"text": "{}"}}"#, text);
            write_string(&mut chat_packet_buf, &content);
        }
        ProtocolVersion::V1_20_4 | ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => {
            write_nbt_string(&mut chat_packet_buf, &text);
        }
    }

    write_bool(&mut chat_packet_buf, false);

    send_packet(
        socket,
        match version {
            ProtocolVersion::V1_19_1 => 0x62,
            ProtocolVersion::V1_19_3 => 0x60,
            ProtocolVersion::V1_19_4 | ProtocolVersion::V1_20 => 0x64,
            ProtocolVersion::V1_20_4 => 0x69,
            ProtocolVersion::V1_20_6 | ProtocolVersion::V1_21 => 0x6c,
        },
        &chat_packet_buf,
        compression_enabled,
    )
    .await?;

    Ok(())
}
