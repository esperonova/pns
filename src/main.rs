mod http;
mod protocol;
mod proxy;

#[tokio::main]
async fn main() {
    let db = sled::open("db").unwrap();

    futures_util::try_join!(proxy::run(&db), http::run(db.clone()),).expect("Failed to run");
}
