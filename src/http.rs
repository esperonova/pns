use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::sync::Arc;

const CONTENT_TYPE_TEXT: hyper::header::HeaderValue =
    hyper::header::HeaderValue::from_static("text/plain;charset=utf-8");

#[derive(Debug)]
struct UserError(hyper::Response<String>);

impl UserError {
    pub fn simple(code: hyper::StatusCode, body: &str) -> Self {
        let mut res = hyper::Response::new(body.to_owned());
        res.headers_mut()
            .insert(hyper::header::CONTENT_TYPE, CONTENT_TYPE_TEXT);
        *res.status_mut() = code;

        UserError(res)
    }
}

impl std::fmt::Display for UserError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "UserError({}): {}", self.0.status(), self.0.body())
    }
}

impl std::error::Error for UserError {}

#[derive(Deserialize)]
struct ChangePasswordParams<'a> {
    pub username: Cow<'a, str>,
    pub password: Cow<'a, str>,
    pub new_password: Cow<'a, str>,
}

#[derive(Serialize)]
struct UserInfo {
    pub username: String,
}

async fn handle_request_inner(
    req: hyper::Request<hyper::body::Incoming>,
    ctx: Arc<Context>,
) -> Result<hyper::Response<String>, anyhow::Error> {
    use http_body_util::BodyExt;

    let db = &ctx.db;

    match req.uri().path() {
        "/api/change_password" => match *req.method() {
            hyper::Method::POST => {
                let body = req.into_body().collect().await?.to_bytes();
                let body = std::str::from_utf8(&body)?;
                let body: ChangePasswordParams =
                    serde_querystring::from_str(&body, serde_querystring::ParseMode::UrlEncoded)?;
                let ChangePasswordParams {
                    password,
                    new_password,
                    ..
                } = body;

                let lookup_username = body.username.to_ascii_lowercase();
                let passhash = db.get(&lookup_username)?.ok_or_else(|| {
                    UserError::simple(hyper::StatusCode::BAD_REQUEST, "Uzanto ne trovita")
                })?;
                let passhash = String::from_utf8(passhash.to_vec())?;

                let ok = tokio::task::spawn_blocking(move || {
                    bcrypt::verify(password.as_bytes(), &passhash)
                })
                .await??;

                if !ok {
                    return Err(UserError::simple(
                        hyper::StatusCode::FORBIDDEN,
                        "Malĝusta pasvorto",
                    )
                    .into());
                }

                let new_passhash = tokio::task::spawn_blocking(move || {
                    bcrypt::hash(new_password.as_bytes(), bcrypt::DEFAULT_COST)
                })
                .await??;

                db.insert(&lookup_username, new_passhash.as_bytes())?;

                Ok(hyper::Response::builder().body("okej".to_owned())?)
            }
            _ => Ok(hyper::Response::builder()
                .status(hyper::StatusCode::METHOD_NOT_ALLOWED)
                .body("Method Not Allowed".to_owned())?),
        },
        "/api/users" => match *req.method() {
            hyper::Method::GET => {
                let admin_password = ctx
                    .admin_password
                    .as_ref()
                    .ok_or(anyhow::anyhow!("no admin password set"))?;

                let credentials = <headers::Authorization<headers::authorization::Basic> as headers::Header>::decode(&mut req.headers().get_all(hyper::header::AUTHORIZATION).iter())?.0;

                if credentials.username() == "admin" && credentials.password() == admin_password {
                    let response: Result<Vec<_>, _> = db
                        .iter()
                        .map(|res| {
                            res.map_err(anyhow::Error::from).and_then(|(key, _)| {
                                Ok(UserInfo {
                                    username: std::str::from_utf8(&key)?.to_owned(),
                                })
                            })
                        })
                        .collect();
                    let response = response?;

                    let response = serde_json::to_string(&response)?;

                    Ok(hyper::Response::builder()
                        .header(hyper::header::CONTENT_TYPE, "application/json")
                        .body(response)?)
                } else {
                    Ok(hyper::Response::builder()
                        .status(hyper::StatusCode::FORBIDDEN)
                        .body("forbidden".to_owned())?)
                }
            }
            _ => Ok(hyper::Response::builder()
                .status(hyper::StatusCode::METHOD_NOT_ALLOWED)
                .body("Method Not Allowed".to_owned())?),
        },
        "/change_password" => match *req.method() {
            hyper::Method::GET => Ok(hyper::Response::builder()
                .header(hyper::header::CONTENT_TYPE, "text/html;charset=utf-8")
                .body(include_str!("./res/change_password.html").to_owned())?),
            _ => Ok(hyper::Response::builder()
                .status(hyper::StatusCode::METHOD_NOT_ALLOWED)
                .body("Method Not Allowed".to_owned())?),
        },
        _ => Ok(hyper::Response::builder()
            .status(hyper::StatusCode::NOT_FOUND)
            .body("Not Found".to_owned())?),
    }
}

async fn handle_request(
    req: hyper::Request<hyper::body::Incoming>,
    ctx: Arc<Context>,
) -> Result<hyper::Response<String>, anyhow::Error> {
    match handle_request_inner(req, ctx).await {
        Ok(res) => Ok(res),
        Err(err) => match err.downcast::<UserError>() {
            Ok(user_err) => Ok(user_err.0),
            Err(err) => {
                eprintln!("Error while handling request: {:?}", err);
                Ok(hyper::Response::builder()
                    .status(hyper::StatusCode::INTERNAL_SERVER_ERROR)
                    .body("Internal Server Error".to_owned())?)
            }
        },
    }
}

struct Context {
    db: sled::Db,
    admin_password: Option<String>,
}

pub async fn run(db: sled::Db) -> Result<(), anyhow::Error> {
    let listener = tokio::net::TcpListener::bind("0.0.0.0:28066").await?;

    let admin_password = match std::env::var("PNS_ADMIN_PASSWORD") {
        Ok(value) => Some(value),
        Err(std::env::VarError::NotPresent) => None,
        Err(err) => return Err(err.into()),
    };

    let context = Arc::new(Context { db, admin_password });

    loop {
        match listener.accept().await {
            Ok((socket, _)) => {
                let context = context.clone();
                tokio::spawn(async move {
                    println!("handling web connection");
                    if let Err(err) = hyper::server::conn::http1::Builder::new()
                        .serve_connection(
                            hyper_util::rt::TokioIo::new(socket),
                            hyper::service::service_fn(|req| handle_request(req, context.clone())),
                        )
                        .await
                    {
                        eprintln!("Failed while handling connection: {:?}", err);
                    }
                });
            }
            Err(err) => eprintln!("Failed to accept client: {:?}", err),
        }
    }
}
